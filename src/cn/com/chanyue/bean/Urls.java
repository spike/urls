package cn.com.chanyue.bean;

import org.nutz.dao.entity.annotation.*;

/**
 * 
 * @author Howe
 *
 */
@Table("tb_chanyue_url_urls")
public class Urls {

	/**
	 * 短Key
	 */
	@Column("shortKey")
	private String shortKey;

	public String getShortKey() {
		return shortKey;
	}

	public void setShortKey(String shortKey) {
		this.shortKey = shortKey;
	}

	/**
	 * 链接内容标题
	 */
	@Column("title")
	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 长网址
	 */
	@Column("longUrl")
	private String longUrl;

	public String getLongUrl() {
		return longUrl;
	}

	public void setLongUrl(String longUrl) {
		this.longUrl = longUrl;
	}

	/**
	 * 添加时间
	 */
	@Column("addTime")
	private java.util.Date addTime;

	public java.util.Date getAddTime() {
		return addTime;
	}

	public void setAddTime(java.util.Date addTime) {
		this.addTime = addTime;
	}
}