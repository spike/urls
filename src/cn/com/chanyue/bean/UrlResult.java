package cn.com.chanyue.bean;

import org.nutz.lang.Strings;

public class UrlResult {

	private boolean result;
	private String shortUrl;
	private String longUrl;
	
	public static UrlResult Long(String url) {
		return new UrlResult(null, url);
	}
	
	public static UrlResult Short(String url) {
		return new UrlResult(url, null);
	}
	
	public UrlResult(){}
	
	public UrlResult(String shortUrl, String longUrl) {
		super();
		this.shortUrl = shortUrl;
		this.longUrl = longUrl;
		this.result = !Strings.isBlank(shortUrl) || !Strings.isBlank(longUrl);
	}

	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public String getShortUrl() {
		return shortUrl;
	}
	public void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}
	public String getLongUrl() {
		return longUrl;
	}
	public void setLongUrl(String longUrl) {
		this.longUrl = longUrl;
	}
	
	
}
